from rest_framework import serializers
from .models import *

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'

class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = '__all__'

class BookSerializer(serializers.ModelSerializer):
    author = serializers.SlugRelatedField(queryset=Author.objects.all(), many=True, slug_field='name')
    genre = serializers.SlugRelatedField(queryset=Genre.objects.all(), many=True, slug_field='name')
    class Meta:
        model = Book
        fields = '__all__'


