# Кириллов Иван Сергеевич
- Вариант 2

- Задание:
> Реализовать все модели данных указанных в задании выше.
> Написать API к ними, на добавление, удаление, редактирование, получение списка всех объектов
> Реализовать токенную аутентификацию
> Деплой приложения. Сделать пользвателя для авторизации username:admin password:admin
> Ссылку на проект в gitlab + ссылка на задеплоенное приложение в файл final.txt
>
> Тема "Библиотека". Сущности: Автор книги, Книга, Жанр

- Дата: 15.07.22

- **URLs**


|   https://vankirillov.pythonanywhere.com/admin/           | admin         |
|-----------------------------------------------------------|---------------|
|   https://vankirillov.pythonanywhere.com/auth/token/login |  регистрация  |
| https://vankirillov.pythonanywhere.com/api/v1/book/       | список книг   |
| https://vankirillov.pythonanywhere.com/api/v1/book/1/     | книг 1        |
| https://vankirillov.pythonanywhere.com/api/v1/author/     | список авторов|
| https://vankirillov.pythonanywhere.com/api/v1/author/1/   | автор 1       |
| https://vankirillov.pythonanywhere.com/api/v1/genre/      | список жанров |
| https://vankirillov.pythonanywhere.com/api/v1/genre/1/    | жанр 1        |
